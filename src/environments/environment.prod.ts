export const environment = {
  production: true,
  //Coverage validator web service
  coverageWebServiceUrl: 'https://webhook.site/',
  coverageWebServiceTokein: 'd748e0c1-9335-4b9a-83cc-74febc45a594',
};
