//Libraries
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

//Components
import { ClientComponent } from './modules/client/client/client.component';

//App routs
const routes: Routes = [
  {
    path: 'client', //Client route
    component: ClientComponent,
    children: [
      {
        path: '',
        //Importing the client module
        loadChildren: () =>
          import('./modules/client/client.module').then(
            (module) => module.ClientModule
          ),
      },
    ],
  },
  {
    path: '',
    redirectTo: 'client',
    pathMatch: 'full',
  },
  {
    path: '**', //Default route
    redirectTo: 'client',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
