//Libraries
import { Component, OnInit, Input } from '@angular/core';

//Models
import { Client } from 'src/app/models/client.model';
import { Coverage } from 'src/app/models/coverage.model';

//Services
import { CoverageService } from 'src/app/services/coverage/coverage.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
})
export class ProfileComponent implements OnInit {
  //Privates variables
  //Demo client
  private testClient: Client = {
    profileImage:
      'https://i.pinimg.com/474x/6f/6a/d8/6f6ad850a31e051d40d309f675df27e2.jpg',
    entity: {
      firstname: 'Ariel',
      lastname: 'Feliz',
    },
  };
  //Demo coverages
  private demoCoverages: Array<Coverage> = [
    {
      description: 'Para tu mejor amigo.',
      active: true,
    },
    {
      description: 'Por si te enfermas.',
      active: false,
    },
    {
      description: 'Para tu bici. ',
      active: true,
    },
  ];

  //Demo other coverages
  private demoDiscoverCoverages: Array<Coverage> = [
    {
      description: 'Por lo que conduces',
    },
    {
      description: 'Por si chocas',
    },
  ];
  //Publics variables
  //List of coverages
  coverages: Array<Coverage>;
  discoverCoverages: Array<Coverage>;

  constructor(private coverageService: CoverageService) {
    this.coverages = this.demoCoverages;
    this.client = this.testClient;
    this.discoverCoverages = this.demoDiscoverCoverages;
  }

  //Inputs
  @Input() client: Client;

  ngOnInit(): void {}

  //Privates functions

  //Publics fucntions

  //Validate coverage
  /* Send a coverage to validate with the client information */
  /* Inputs:
   *-covegare: Coverage
   */
  validateCoverage(covegare: Coverage) {
    let covegareDescription: string = covegare.description || '';
    let clientName: string =
      this.client.entity.firstname + '.' + this.client.entity.lastname;
    let date: Date = new Date();
    this.coverageService
      .validateCoverage(
        clientName.toLocaleLowerCase(),
        covegareDescription,
        date.getTime().toString()
      )
      .subscribe(
        (res: any) => {
          covegare.active = true;
          console.log(res);
        },
        (error: any) => {
          covegare.active = false;
          console.error(error);
        }
      );
  }
}
