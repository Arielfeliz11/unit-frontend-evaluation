//Libraries
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

//Modules
import { ProfileComponent } from './profile/profile.component';

//Routes
import { ClientRoutes } from './client.routing';

@NgModule({
  declarations: [ProfileComponent],
  imports: [CommonModule, RouterModule.forChild(ClientRoutes), FormsModule],
})
export class ClientModule {}
