//Routes
import { Routes } from '@angular/router';

//Component
import { ProfileComponent } from './profile/profile.component';

export const ClientRoutes: Routes = [{ path: '', component: ProfileComponent }];
