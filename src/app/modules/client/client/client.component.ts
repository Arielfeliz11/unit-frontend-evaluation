//Libraries
import { Component, OnInit } from '@angular/core';

//Models
import { Client } from 'src/app/models/client.model';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.css'],
})
export class ClientComponent implements OnInit {
  //Private variables
  private testClient: Client = {
    profileImage:
      'https://i.pinimg.com/474x/6f/6a/d8/6f6ad850a31e051d40d309f675df27e2.jpg',
    entity: {
      firstname: 'Ariel',
      lastname: 'Feliz',
    },
  };
  //Publics variables
  client: Client;
  constructor() {}

  ngOnInit(): void {
    this.client = new Client(this.testClient);
  }
}
