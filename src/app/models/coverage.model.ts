//Model interface
export interface ICoverage {
  description?: string;
  active?: boolean;
}

//Model interface
export class Coverage {
  description?: string;
  active?: boolean;
  constructor(thisClass: any) {
    //Set defaults values
    this.description = '';
    //Set dynamics values
    Object.keys(thisClass).forEach((property: string) => {
      this[property as keyof Coverage] = thisClass[property as keyof Coverage];
    });
  }
}
