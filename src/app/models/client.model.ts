export interface IClient {
  profileImage?: string;
  entity: IEntity;
}

export class Client {
  profileImage?: string;
  entity: IEntity;

  constructor(thisClass: any) {
    //Set defaults values
    this.entity = {};
    //Set dynamics values
    Object.keys(thisClass).forEach((property: string) => {
      this[property as keyof Client] = thisClass[property as keyof Client];
    });
  }
}

//Private interface
interface IEntity {
  firstname?: string;
  lastname?: string;
}
