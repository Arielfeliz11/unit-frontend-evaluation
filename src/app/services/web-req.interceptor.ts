//Libraries
import { Injectable } from '@angular/core';
import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
  HttpErrorResponse,
} from '@angular/common/http';
import { Observable, throwError, Subject } from 'rxjs';
import { catchError, tap, switchMap } from 'rxjs/operators';

//Enviroemnt
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class WebReqInterceptor implements HttpInterceptor {
  //Private variables
  private interceptTry: number = 0;
  readonly coverageWebServiceUrl: string = environment.coverageWebServiceUrl;

  constructor() {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<any> {
    // Handle the request
    request = this.addAccessControl(request);
    // call next() and handle the response
    return next.handle(request).pipe(
      catchError((error: HttpErrorResponse) => {
        return throwError(error);
      })
    );
  }

  addAccessControl(request: HttpRequest<any>) {
    return request.clone({
      setHeaders: {
        'Access-Control-Allow-Headers': 'Content-Type',
        'Access-Control-Allow-Methods': 'GET POST',
        'Access-Control-Allow-Origin': '*',
      },
    });
  }
}
