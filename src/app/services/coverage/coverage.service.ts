//Libraries
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

//Enviriment variables
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class CoverageService {
  //Private variables

  //Coverage validator web service url
  readonly coverageWebServiceUrl: string;
  //Coverage validator web service url
  readonly coverageWebServiceToken: string;

  constructor(private http: HttpClient, private router: Router) {
    this.coverageWebServiceUrl = environment.coverageWebServiceUrl;
    this.coverageWebServiceToken = environment.coverageWebServiceToken;
  }
  //Private functions

  //Publics functions
  //Validate coverage
  /*Validate a client with a coverage to determine if this coverage is available or not for the client
   *Inputs
   *-client: string - Fullname of the client
   *-coverage: string - Coverage name
   *-date: string - Timestamp of the actual date
   */
  validateCoverage(
    client: string,
    coverage: string,
    date: string
  ): Observable<any> {
    let serviceUrl =
      this.coverageWebServiceUrl +
      this.coverageWebServiceToken +
      '?nickname_developer=' +
      client +
      '&product_name=' +
      coverage +
      '&timestamp=' +
      date;
    serviceUrl = encodeURI(serviceUrl);
    return this.http.post<any>(serviceUrl, '');
  }
}
